#[cfg(feature = "wasm-bindings")]
use crate::client::BoxedProfile;
use crate::{Product, Profile};
use serde_json::{json, Value};
use std::collections::HashMap;
#[cfg(feature = "wasm-bindings")]
use wasm_bindgen::prelude::wasm_bindgen;

mod products {
    use crate::{Mode, Product};
    use std::borrow::Cow;

    pub const REGIONAL_TRAIN: Product = Product {
        id: Cow::Borrowed("regional-train"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[8]),
        name: Cow::Borrowed("regional train"),
        short: Cow::Borrowed("regional train"),
    };
    pub const URBAN_TRAIN: Product = Product {
        id: Cow::Borrowed("urban-train"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[16]),
        name: Cow::Borrowed("urban train"),
        short: Cow::Borrowed("urban train"),
    };
    pub const SUBWAY: Product = Product {
        id: Cow::Borrowed("subway"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[128]),
        name: Cow::Borrowed("subway"),
        short: Cow::Borrowed("subway"),
    };
    pub const TRAM: Product = Product {
        id: Cow::Borrowed("tram"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[256]),
        name: Cow::Borrowed("tram"),
        short: Cow::Borrowed("tram"),
    };
    pub const BUS: Product = Product {
        id: Cow::Borrowed("bus"),
        mode: Mode::Bus,
        bitmasks: Cow::Borrowed(&[32]),
        name: Cow::Borrowed("bus"),
        short: Cow::Borrowed("bus"),
    };
    pub const DIAL_A_RIDE: Product = Product {
        id: Cow::Borrowed("dial-a-ride"),
        mode: Mode::Taxi,
        bitmasks: Cow::Borrowed(&[512]),
        name: Cow::Borrowed("dial-a-ride"),
        short: Cow::Borrowed("dial-a-ride"),
    };
    pub const LONG_DISTANCE_TRAIN: Product = Product {
        id: Cow::Borrowed("long-distance-train"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[4]),
        name: Cow::Borrowed("long-distance train"),
        short: Cow::Borrowed("long-distance train"),
    };
    pub const ICE: Product = Product {
        id: Cow::Borrowed("express-train"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[1]),
        name: Cow::Borrowed("ICE"),
        short: Cow::Borrowed("ICE"),
    };
    pub const EC: Product = Product {
        id: Cow::Borrowed("ec-ic"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[2]),
        name: Cow::Borrowed("EC/IC"),
        short: Cow::Borrowed("EC/IC"),
    };

    pub const PRODUCTS: &[&Product] = &[
        &REGIONAL_TRAIN,
        &URBAN_TRAIN,
        &SUBWAY,
        &TRAM,
        &BUS,
        &DIAL_A_RIDE,
        &LONG_DISTANCE_TRAIN,
        &ICE,
        &EC,
    ];
}

#[derive(Debug)]
pub struct MobilNrwProfile;

impl Profile for MobilNrwProfile {
    fn url(&self) -> &'static str {
        "https://nrw.hafas.de/bin/mgate.exe"
    }
    fn language(&self) -> &'static str {
        "de"
    }
    fn timezone(&self) -> chrono_tz::Tz {
        chrono_tz::Europe::Berlin
    }
    fn checksum_salt(&self) -> Option<&'static str> {
        None
    }
    fn refresh_journey_use_out_recon_l(&self) -> bool {
        true
    }

    fn products(&self) -> &'static [&'static Product] {
        products::PRODUCTS
    }

    fn prepare_body(&self, req_json: &mut Value) {
        req_json["client"] = json!({"type":"IPH","id":"DB-REGIO-NRW","v":"6000300","name":"NRW"});
        req_json["ver"] = json!("1.34");
        req_json["auth"] = json!({"type":"AID","aid":"Kdf0LNRWYg5k3499"});
    }

    fn prepare_headers(&self, headers: &mut HashMap<&str, &str>) {
        headers.insert("User-Agent", "my-awesome-e5f276d8fe6cprogram");
    }

    fn price_currency(&self) -> &'static str {
        "EUR"
    }
}

#[cfg(feature = "wasm-bindings")]
#[wasm_bindgen]
impl MobilNrwProfile {
    #[wasm_bindgen(constructor)]
    pub fn wasm_new() -> BoxedProfile {
        Self.into()
    }
}

#[cfg(test)]
mod test {
    use std::error::Error;

    use crate::profile::test::{check_journey, check_search};

    use super::*;

    #[tokio::test]
    async fn test_search() -> Result<(), Box<dyn Error>> {
        check_search(MobilNrwProfile {}, "Kreu", "Kreuztal").await
    }

    #[tokio::test]
    async fn test_path_available() -> Result<(), Box<dyn Error>> {
        check_journey(MobilNrwProfile {}, "8000076", "8000001").await
    }
}
