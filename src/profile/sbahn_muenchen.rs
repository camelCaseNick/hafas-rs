#[cfg(feature = "wasm-bindings")]
use crate::client::BoxedProfile;
use crate::{Product, Profile};
use serde_json::{json, Value};
use std::collections::HashMap;
#[cfg(feature = "wasm-bindings")]
use wasm_bindgen::prelude::wasm_bindgen;

mod products {
    use crate::{Mode, Product};
    use std::borrow::Cow;

    pub const ICE: Product = Product {
        id: Cow::Borrowed("ice"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[1]),
        name: Cow::Borrowed("InterCityExpress"),
        short: Cow::Borrowed("ICE"),
    };
    pub const IC: Product = Product {
        id: Cow::Borrowed("ic-ec"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[2]),
        name: Cow::Borrowed("InterCity/EuroCity"),
        short: Cow::Borrowed("IC/EC"),
    };
    pub const IRE: Product = Product {
        id: Cow::Borrowed("ir-d"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[4]),
        name: Cow::Borrowed("Interregio/Schnellzug"),
        short: Cow::Borrowed("IRE"),
    };
    pub const RE: Product = Product {
        id: Cow::Borrowed("region"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[8]),
        name: Cow::Borrowed("Regio- und Nahverkehr"),
        short: Cow::Borrowed("RE/RB"),
    };
    pub const S: Product = Product {
        id: Cow::Borrowed("sbahn"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[16]),
        name: Cow::Borrowed("S-Bahn"),
        short: Cow::Borrowed("S"),
    };
    pub const BUS: Product = Product {
        id: Cow::Borrowed("bus"),
        mode: Mode::Bus,
        bitmasks: Cow::Borrowed(&[32]),
        name: Cow::Borrowed("Bus"),
        short: Cow::Borrowed("Bus"),
    };
    pub const U: Product = Product {
        id: Cow::Borrowed("ubahn"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[128]),
        name: Cow::Borrowed("U-Bahn"),
        short: Cow::Borrowed("U"),
    };
    pub const TRAM: Product = Product {
        id: Cow::Borrowed("tram"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[256]),
        name: Cow::Borrowed("Straßenbahn"),
        short: Cow::Borrowed("Tram"),
    };
    pub const SAMMELTAXI: Product = Product {
        id: Cow::Borrowed("on-call"),
        mode: Mode::Taxi,
        bitmasks: Cow::Borrowed(&[512]),
        name: Cow::Borrowed("Anrufsammeltaxi"),
        short: Cow::Borrowed("Sammeltaxi"),
    };

    pub const PRODUCTS: &[&Product] = &[&ICE, &IC, &IRE, &RE, &S, &BUS, &U, &TRAM, &SAMMELTAXI];
}

#[derive(Debug)]
pub struct SBahnMuenchenProfile;

impl Profile for SBahnMuenchenProfile {
    fn url(&self) -> &'static str {
        "https://s-bahn-muenchen.hafas.de/bin/540/mgate.exe"
    }
    fn language(&self) -> &'static str {
        "en"
    }
    fn checksum_salt(&self) -> Option<&'static str> {
        Some("ggnvMVV8RTt67gh1")
    }
    fn timezone(&self) -> chrono_tz::Tz {
        chrono_tz::Europe::Berlin
    }
    fn mic_mac(&self) -> bool {
        true
    }
    fn refresh_journey_use_out_recon_l(&self) -> bool {
        true
    }

    fn products(&self) -> &'static [&'static Product] {
        products::PRODUCTS
    }

    fn prepare_body(&self, req_json: &mut Value) {
        req_json["client"] =
            json!({"type":"IPH","id":"DB-REGIO-MVV","v":"5010100","name":"MuenchenNavigator"});
        req_json["ver"] = json!("1.34");
        req_json["ext"] = json!("DB.R15.12.a");
        req_json["auth"] = json!({"type":"AID","aid":"d491MVVhz9ZZts23"});
    }

    fn prepare_headers(&self, headers: &mut HashMap<&str, &str>) {
        headers.insert("User-Agent", "my-awesome-e5f276d8fe6cprogram");
    }

    fn price_currency(&self) -> &'static str {
        "EUR"
    }
}

#[cfg(feature = "wasm-bindings")]
#[wasm_bindgen]
impl SBahnMuenchenProfile {
    #[wasm_bindgen(constructor)]
    pub fn wasm_new() -> BoxedProfile {
        Self.into()
    }
}

#[cfg(test)]
mod test {
    use std::error::Error;

    use crate::profile::test::{check_journey, check_search};

    use super::*;

    #[tokio::test]
    async fn test_search() -> Result<(), Box<dyn Error>> {
        check_search(SBahnMuenchenProfile {}, "Arena", "Allianz-Arena, München").await
    }

    #[tokio::test]
    async fn test_path_available() -> Result<(), Box<dyn Error>> {
        check_journey(SBahnMuenchenProfile {}, "8004158", "8000261").await
    }
}
