#[cfg(feature = "wasm-bindings")]
use crate::client::BoxedProfile;
use crate::{Product, Profile};
use serde_json::{json, Value};
use std::collections::HashMap;
#[cfg(feature = "wasm-bindings")]
use wasm_bindgen::prelude::wasm_bindgen;

mod products {
    use crate::{Mode, Product};
    use std::borrow::Cow;

    pub const BUS: Product = Product {
        id: Cow::Borrowed("bus"),
        mode: Mode::Bus,
        bitmasks: Cow::Borrowed(&[1, 16]),
        name: Cow::Borrowed("Bus"),
        short: Cow::Borrowed("Bus"),
    };
    pub const TRAIN_EXP: Product = Product {
        id: Cow::Borrowed("express-train"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[2]),
        name: Cow::Borrowed("High-speed train"),
        short: Cow::Borrowed("Train"),
    };
    pub const TRAIN_REG: Product = Product {
        id: Cow::Borrowed("regional-train"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[4]),
        name: Cow::Borrowed("Regional train"),
        short: Cow::Borrowed("Train"),
    };
    pub const ZUG: Product = Product {
        id: Cow::Borrowed("local-train"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[8]),
        name: Cow::Borrowed("Nahverkehrszug"),
        short: Cow::Borrowed("Zug"),
    };
    pub const FERRY: Product = Product {
        id: Cow::Borrowed("ferry"),
        mode: Mode::Watercraft,
        bitmasks: Cow::Borrowed(&[32]),
        name: Cow::Borrowed("Ferry"),
        short: Cow::Borrowed("Ferry"),
    };
    pub const SUBWAY: Product = Product {
        id: Cow::Borrowed("subway"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[64]),
        name: Cow::Borrowed("Subway"),
        short: Cow::Borrowed("Subway"),
    };
    pub const TRAM: Product = Product {
        id: Cow::Borrowed("tram"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[128]),
        name: Cow::Borrowed("Tram"),
        short: Cow::Borrowed("Tram"),
    };
    pub const ON_DEMAND: Product = Product {
        id: Cow::Borrowed("on-demand"),
        mode: Mode::Bus,
        bitmasks: Cow::Borrowed(&[256]),
        name: Cow::Borrowed("On-demand traffic"),
        short: Cow::Borrowed("on demand"),
    };

    pub const PRODUCTS: &[&Product] = &[
        &BUS, &TRAIN_EXP, &TRAIN_REG, &ZUG, &FERRY, &SUBWAY, &TRAM, &ON_DEMAND,
    ];
}

#[derive(Debug)]
pub struct VgiProfile;

impl Profile for VgiProfile {
    fn url(&self) -> &'static str {
        "https://fpa.invg.de/bin/mgate.exe"
    }
    fn language(&self) -> &'static str {
        "de"
    }
    fn timezone(&self) -> chrono_tz::Tz {
        chrono_tz::Europe::Berlin
    }
    fn checksum_salt(&self) -> Option<&'static str> {
        None
    }
    fn refresh_journey_use_out_recon_l(&self) -> bool {
        true
    }

    fn products(&self) -> &'static [&'static Product] {
        products::PRODUCTS
    }

    fn prepare_body(&self, req_json: &mut Value) {
        req_json["client"] =
            json!({"type":"IPH","id":"INVG","v":"1040000","name":"invgPROD-APPSTORE-LIVE"});
        req_json["ver"] = json!("1.39");
        req_json["auth"] = json!({"type":"AID","aid":"GITvwi3BGOmTQ2a5"});
    }

    fn prepare_headers(&self, headers: &mut HashMap<&str, &str>) {
        headers.insert("User-Agent", "my-awesome-e5f276d8fe6cprogram");
    }

    fn price_currency(&self) -> &'static str {
        "EUR"
    }
}

#[cfg(feature = "wasm-bindings")]
#[wasm_bindgen]
impl VgiProfile {
    #[wasm_bindgen(constructor)]
    pub fn wasm_new() -> BoxedProfile {
        Self.into()
    }
}

#[cfg(test)]
mod test {
    use std::error::Error;

    use crate::profile::test::{check_journey, check_search};

    use super::*;

    #[tokio::test]
    async fn test_search() -> Result<(), Box<dyn Error>> {
        check_search(VgiProfile {}, "Ingol", "Ingolstadt Audi").await
    }

    #[tokio::test]
    async fn test_path_available() -> Result<(), Box<dyn Error>> {
        check_journey(VgiProfile {}, "8000183", "84999").await
    }
}
